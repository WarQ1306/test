<?php


namespace App\Command;


use App\Services\WorkersService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;



class ShowWorks extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'company:employee';

    protected function configure()
    {
        $this->addArgument('worker',InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = new WorkersService();
        $result = $service->getWorks((string) $input->getArgument('worker'));
        $output->write($result);
    }

}