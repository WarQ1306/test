<?php


namespace App\Services;


use App\Workers\Designer;
use App\Workers\Manager;
use App\Workers\Programmer;
use App\Workers\Tester;

class WorkersService
{
    private $workers = [
        'tester' => Tester::class,
        'programmer' => Programmer::class,
        'designer' => Designer::class,
        'manager' => Manager::class
    ];

    private function checkWorker(string $workerName)
    {
        if (!isset($this->workers[$workerName])) {
            throw new \Exception('This worker is not exists');
        }
    }

    public function canWork(string $workerName, string $workName): bool
    {
        $this->checkWorker($workerName);
        $worker = new $this->workers[$workerName];
        return method_exists($worker, $workName);
    }

    public function getWorks(string $workerName)
    {
        $this->checkWorker($workerName);
        $worker = new $this->workers[$workerName];
        $methods = get_class_methods($worker);
        $works = '';
        foreach ($methods as $work) {
            $works .= " $work ";
        }
        return $works;
    }
}